using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CointSpawn : MonoBehaviour
{
    public GameObject Moneda;
    public int NumeroMonedas;
    public int FuerzaSalida;
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.W) && collision.gameObject.CompareTag("Player"))
        {
            int k = 0;
            while (k < NumeroMonedas)
            {
                GameObject RB= Instantiate(Moneda, transform.position + new Vector3(0, 100, 0), Quaternion.identity);
                RB.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, FuerzaSalida), ForceMode2D.Impulse);
                k++;
            }
        }
    }
}
