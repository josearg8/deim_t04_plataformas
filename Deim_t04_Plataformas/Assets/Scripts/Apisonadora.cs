using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apisonadora : MonoBehaviour
{
    public Animator AnimatorApisonadora;
    public GameObject GameOverPanel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AnimatorApisonadora.SetBool("PlayerEnter", true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameOverPanel.SetActive(true);
            GameOver.Vida = 0;
            GameOver.Muerto = true;
            Time.timeScale = 0;
            PlayerController.Pausa = true;
        }
    }

}
