using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Mov. Personaje
    public Animator AnimatorCharacter;
    public float MovementSpeed;
    private float Movement = 1;

    //Salto
    private Rigidbody2D RB;
    private float JumpCount;
    public float JumpForce = 1;

    //RayCast
    public LayerMask Ground;
    public float DistanciaRay;

    //Pausa
    public static bool Pausa;

    void Start()
    {
        RB = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        //Mov. A/D
        if(Input.GetKey(KeyCode.A))
        {
            AnimatorCharacter.SetBool("Run", true);
            GetComponent<SpriteRenderer>().flipX = true;
            transform.position += new Vector3(-Movement, 0, 0) * Time.deltaTime * MovementSpeed; 
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            AnimatorCharacter.SetBool("Run", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            AnimatorCharacter.SetBool("Run", true);
            GetComponent<SpriteRenderer>().flipX = false;
            transform.position += new Vector3(Movement, 0, 0) * Time.deltaTime * MovementSpeed; 
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            AnimatorCharacter.SetBool("Run", false);
        }

        //Salto
        if (Input.GetButtonDown("Jump") && Mathf.Abs(RB.velocity.y) < 0.001f && Ground())
        {
            AnimatorCharacter.SetBool("Jump", true);
            RB.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
            JumpCount++;
        }
        else if (RB.velocity.y <= 0 && Ground())
        {
            AnimatorCharacter.SetBool("Jump", false);
            JumpCount = 0;
        }

        //Caida
        if (RB.velocity.y < 0)
        {
            AnimatorCharacter.SetBool("Falling", true);
        }
        else if (RB.velocity.y > -2500)
        {
            AnimatorCharacter.SetBool("Falling", false);
        }

        //RayCast Ground
        bool Ground()
        {
            Vector2 position = transform.position;
            Vector2 direction = Vector2.down;
            float distance =200;

            RaycastHit2D hit = Physics2D.Raycast(position, direction, distance,LayerMask.GetMask ("Ground"));

            if (hit.collider != null)
            {
                return true;
            }
            return false;

        }
    }
}
