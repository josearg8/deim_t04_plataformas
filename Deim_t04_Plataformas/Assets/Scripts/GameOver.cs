using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public static int Vida = 1;

    public  GameObject GameOverPanel;

    public static bool Muerto;

    private void Start()
    {
        Muerto = false;
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Enemys")) {
            Vida--;
            if (Vida == 0) {
                Muerto = true;
                GameOverPanel.SetActive(true);
                Time.timeScale = 0;
                PlayerController.Pausa = true;
            }
        }
    }
}
