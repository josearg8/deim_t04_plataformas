using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestLocator : MonoBehaviour
{
    public float DistanciaRay;
    public LayerMask Chest;

    

    void Start()
    {
        
    }

    void Update()
    {
        if (Chest())
        {
            
        }

        bool Chest()
        {
            Vector2 position = transform.position;
            Vector2 direction1 = Vector2.right;
            Vector2 direction2 = Vector2.left;
            float distance = 200;

            RaycastHit2D hit = Physics2D.Raycast(position, direction1, distance, LayerMask.GetMask("Chest"));
            if (hit.collider != null&& Input.GetKeyDown(KeyCode.W))
            {
                hit.collider.GetComponent<Animator>().SetBool("Open", true);
                Destroy(hit.collider);  
            }

            hit = Physics2D.Raycast(position, direction2, distance, LayerMask.GetMask("Chest"));
            if (hit.collider != null&& Input.GetKeyDown(KeyCode.W))
            {
                hit.collider.GetComponent<Animator>().SetBool("Open", true);
                Destroy(hit.collider);
            }
            
            return false;

            
        }
    }
}
