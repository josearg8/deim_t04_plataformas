using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool GameOver;
    public static int Puntos;
    public Text Puntuacion;

    private void Start()
    {
        GameOver = false;
        Puntos = 0;
    }

    private void Update()
    {
        Puntuacion.text = Puntos.ToString();
    }

    public void Muerte()
    {
        GameOver = true;
    }
}